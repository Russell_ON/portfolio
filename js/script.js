
const overlay = document.getElementById('overlay');
const nav = document.querySelector('nav');
const scrollLinks = document.querySelectorAll('.scroll-links');
const logo = document.querySelector(".logo");
const modals = document.querySelectorAll(".modal");
const buttons = document.querySelectorAll(".close-button");
const navToggle = document.querySelector(".toggle-button");
const font = document.getElementById("fa");
const heroBtn = document.querySelector(".hero-button");
const imgContainer = document.querySelector(".img-container");
const percentages = document.querySelectorAll(".loading-bar");
const innerPercentages = document.querySelectorAll(".inner-percentage");
// const assignment = document.getElementById("assignment");
const subjectContainer = document.querySelector('#project');
const navHeight = nav.getBoundingClientRect().height;
const aboutMe = document.querySelector(".about-me");


const getAge = birthDate => Math.floor((new Date() - new Date(birthDate).getTime()) / 3.15576e+10);


const age = getAge('2000-06-20');

aboutMe.textContent = `Mijn naam is Russell Numo, ${age} jaar oud en ik studeer op de Hogeschool Van Amsterdam Communication Multimedia Design(cmd). In het eerste jaar van mijn opleiding ben ik door verschillende fases heen gegaan waarbij sommige wel positief en andere ook wel negatief. Tijdens mijn opleiding heb ik ontdekt dat ik best wel een creatieve persoon bent die gedreven is voor perfectie. Ik heb gemerkt, dat ik tijdens mijn eerste jaar aan cmd het maken en ontwerpen van schermen wel leuk vind toch ligt mijn ambitie bij het programmeren van deze verschillende schermen. In mijn vrije tijd ben ik me heel erg gaan verdiepen in de programmeer wereld en merkte ik, dat ik het heel leuk begon te worden, daarom ben ik wel vrij wel bijna elke dag bezig met het programmeren van iets en besloot ik ook deze website te maken. De laatste paar loodje van mijn opleiding zijn wel wat zwaarder dan gedacht, maar door mijn gedreven persoonlijkheid zal ik wel altijd iets netjes opleveren.`;





const subjects = [

    {
        id:"0",
        subject: "Human Computer Interaction",
        assignment: "Voor de vak HCI had ik de taak gekregen om een ontwerppaneel te ontwerpen voor het bedrijf pinqponq. De ontwerp moet in een oogoplag begrepen worden, zodat de gebruiker een fijne gebruikservaring heeft bij het customizen van hun tas. Gedurende het project ben door 1 grote iteratie slag gegaan die je ook terug kan zien op mijn miro bord.",
        designP: "Tijdens het ontwerpen van deze schermen heb ik gebruik gemaakt van een paar design principles die wij hebben geleerd tijdens dit project deze zijn",
        bannerimg:{
            src:"hci.png",
            alt:"Human Computer Interaction",
        },
        images: [
            {
               src:"HCI landing page.png",
               alt: "hci",
            },
            {
               src:"hci customize.png",
               alt: "hci",
            },
            {
               src:"afrekenen.png",
               alt:"hci",
            }
        ],
        principles: [
            {
                rule: "Clarity",  
            },
            {
                rule: "Concision",  
            },
            {
                rule: "Familiarity",  
            },
            {
                rule: "Consistency",  
            },
            {
                rule: "Effiency",  
            },
            {
                rule: "Forgiveness",
            },
        ],
        final: "Voor de laatste versie van het ontwerppaneel kan ik u refereren naar mijn ontwerpbord waar u de hele screenflow kunt bekijken met de benodigde annotaties.",
        finalLink: "https://miro.com/app/board/o9J_leAsoJI=/",
    },
    {
        id:"1",
        subject: "Visual Interface Design",
        assignment: "Tijdens dit project heb ik de opdracht gekregen om een boekenzoeker te maken voor de gemeente Amsterdam om het voor studenten van de middelbare scholen makkelijk te kunnen maken om een bepaalde boek te zoeken in de bibliotheek.",
        designP: "Tijdens het ontwerpen van deze schermen heb ik gebruik gemaakt van een paar design principles die wij hebben geleerd tijdens dit project deze zijn",
        bannerimg:{
            src:"vid.png",
            alt:"Visual Interface Design",
        },
        images: [
            {
               src:"vid.png",
               alt: "vid",
            },
            {
               src:"leeftijdcategorie.png",
               alt: "vid",
            },
            {
               src:"overzicht.png",
               alt:"vid",
            }
        ],
        principles: [
            {
                rule: "Clarity",  
            },
            {
                rule: "Concision",  
            },
            {
                rule: "Familiarity",  
            },
            {
                rule: "Consistency",  
            },
            {
                rule: "Effiency",  
            },
        ],
        final: "Voor de laatste versie van het ontwerp heb ik een adobe xd linkje waarin je kan kijken hoe de hele flow in elkaar zit. Klik op de onderstaande link om de prototype te kunnen zien.",
        finalLink: "https://xd.adobe.com/view/e65a4a4a-84df-4751-b3bf-cfd37dc03db2-b309/?fullscreen&hints=off",
    },
    {
        id:"2",
        subject: "Frontend Development",
        assignment: "Voor de vak frontend development kreeg ik de opdracht om een zelf uitgekozen website na te bouwen in mobiele formaat. Voor deze opdracht had ik de bijenkorf website nagemaakt en een paar interactieve elementen er in geimplementeerd",
        designP: "Bij het bouwen van de website heb ik gebruik gemaakt van html,css en Javascript. Tijdens het bouwen probeerde ik zoveel mogelijk gebruik te maken van html 5 tags, zodat de website ook makkelijker toegangkelijk is voor meerdere gebruikers.",
        bannerimg:{
            src:"html.jpg",
            alt:"Frontend Development",
        },
        images: [
            {
                src: "html.jpg",
                alt: "bijenkorf",
            },

        ],
        principles: [
            {
                rule: "HTML5",  
            },
            {
                rule: "Css",  
            },
            {
                rule: "Javascript",  
            },
        ],
        final: "Voor de laatste versie van de website heb ik het gepublished via github pages bekijk de website in mobiele formaat.",
        finalLink: "https://github.com/Zephelion/bijenkorf",
    },
    {
        id:"3",
        subject: "Project Ade X Soundcloud",
        assignment: "In mijn 2e jaar heb ik de taak gekregen om een eigen applicatie te ontwerpen waarbij ik de gebruiker een Responsive Multi Device ervaring aan kan bieden. De app kon van alles zijn. Ik heb hiervoor gekozen voor een hobby die ik in me dagelijks leven doe en, dat is manga's verzamelen. Hierbij heb ik een applicatie ontworpen die het makkelijk maakt voor de gebruiker om manga's te vinden bij te kunnen houden in een lijst en een online marketplace.",
        designP: "De app maakt gebruik van een single activity flow waarbij de gebruiker de app kan gebruiken op zijn telefoon en/of deskstop. Tijdens het ontwerpen van de app heb ik gebruik gemaakt van de design regels die ik door mijn opleiding heb aangeleerd.",
        bannerimg:{
            src:"ade.jpg",
            alt:"manga",
        },
        images: [
            {
                src: "collage1.jpg",
                alt: "login",
            },
            {
                src: "Profiel.png",
                alt: "profile",
            },

        ],
        principles: [
            {
                rule: "Clarity",  
            },
            {
                rule: "Concision",  
            },
            {
                rule: "Familiarity",  
            },
            {
                rule: "Consistency",  
            },
            {
                rule: "Effiency",  
            },
        ],
        final: "De laatste versie van mijn design heb ik gedemonstreerd in een ontwerpvideo die je door op de onderstaande linkje te klikken te kunnen bekijken.",
        finalLink: "https://youtu.be/8mybPPm4UWA",
    },
    {
        id:"4",
        subject: "Project RMDD",
        assignment: "In mijn 2e jaar heb ik de taak gekregen om een eigen applicatie te ontwerpen waarbij ik de gebruiker een Responsive Multi Device ervaring aan kan bieden. De app kon van alles zijn. Ik heb hiervoor gekozen voor een hobby die ik in me dagelijks leven doe en, dat is manga's verzamelen. Hierbij heb ik een applicatie ontworpen die het makkelijk maakt voor de gebruiker om manga's te vinden bij te kunnen houden in een lijst en een online marketplace.",
        designP: "De app maakt gebruik van een single activity flow waarbij de gebruiker de app kan gebruiken op zijn telefoon en/of deskstop. Tijdens het ontwerpen van de app heb ik gebruik gemaakt van de design regels die ik door mijn opleiding heb aangeleerd.",
        bannerimg:{
            src:"manga.png",
            alt:"manga",
        },
        images: [
            {
                src: "startscherm.png",
                alt: "manga",
            },
            {
                src: "manga-modal.png",
                alt: "manga",
            },
            {
                src: "Marketplace.png",
                alt: "manga",
            },

        ],
        principles: [
            {
                rule: "Clarity",  
            },
            {
                rule: "Concision",  
            },
            {
                rule: "Familiarity",  
            },
            {
                rule: "Consistency",  
            },
            {
                rule: "Effiency",  
            },
            {
                rule: "Forgiveness",
            },
            {
                rule: "UI States",  
            },
            {
                rule: "Single Activity Flow",  
            },
        ],
        final: "De laatste versie van mijn design heb ik op een ontwerpbord gezet met de benodigde documentatie om het makkelijk te begrijpen",
        finalLink: "https://miro.com/app/board/o9J_lnec2Qo=/",
    },
    {
        id:"5",
        subject: "Inleiding Programmeren",
        assignment: "Aan het einde van mijn 1ste jaar heb ik de opdracht gekregen om met behulp van Javascript een leuke prototype te kunnen bouwen. Het overkoepelende doel van het vak Inleiding Programmeren is dat je basisprogrammeerkennis en -vaardigheden ontwikkelt waardoor je ideeën kunt verwezenlijken in de vorm van een prototype",
        designP: "De webapp is gemaakt in css en ik maak gebruik van Javascript voor de interactie ",
        bannerimg:{
            src:"javascript.png",
            alt:"javascript",
        },
        images: [
            {
                src: "sonic.png",
                alt: "manga",
            },

        ],
        principles: [
            {
                rule: "Html",  
            },
            {
                rule: "Css",  
            },
            {
                rule: "Javascript",  
            },
        ],
        final: "De laatste versie kan je inzien op mijn gitlab door op de onderstaande link te klikken (clone de repo om de prototype uit te kunnen testen).",
        finalLink: "https://gitlab.com/Russell_ON/sonic",
    },
];

let subjectHtml = ''

const loadSubjects = () => {

    subjects.forEach(subject => {
       
        subjectHtml += `

        <div class="project" data-id="${subject.id}">
            <img src="images/${subject.bannerimg.src}" alt="${subject.bannerimg.alt}">
        </div>
        `;

    });

    subjectContainer.innerHTML = subjectHtml;
}



window.addEventListener('DOMContentLoaded', () => {
    loadSubjects();
    const projects = document.querySelectorAll('.project');
    
    projects.forEach(function(project){
        project.addEventListener("click", function(){
            const id = project.dataset.id;
            openModal(id);
        })
    });
})

const modal = document.getElementById("modal");
const title = modal.querySelector(".modal-container > h2");
const assignment = modal.querySelector("#assignment");
const principle = modal.querySelector("#principle");
const principles = modal.querySelector("#principle-list");
const final = modal.querySelector("#final");
const images = modal.querySelector("#img-container");
const link = modal.querySelector(".modal-link")



const openModal = (id) => {
    // console.log(subjects[id]);
    let project = findProjectById(id);

    title.innerText = project.subject
    assignment.innerText = project.assignment
    principle.innerText = project.designP
    // principles.innerText = project.
    final.innerText = project.final
    link.href = project.finalLink
    let imagesHTML = '';
    let principleHTML = '';
    project.images.forEach(image => {
        console.log(image);
        imagesHTML += `
            <figure class="img-project">
                <img src="images/${image.src}" alt="${image.alt}" />
            </figure>
        `;
    });
    project.principles.forEach(principle => {
        principleHTML += `
        <ul id="principle-list">
            <li>${principle.rule}</li>
        </ul>`;
    });
    images.innerHTML = imagesHTML;
    principles.innerHTML = principleHTML;
    modal.classList.add("modal-block");
}

const findProjectById = (id) => {
    console.log(id);
    return subjects.find((subject) => {
        return subject.id == id    
    });
}



percentages.forEach(function(percentage){

    const widthLoadingbar = percentage.offsetWidth;
    const innerLoadingdbar = percentage.firstElementChild.offsetWidth;

    const procent = Math.round(100 / widthLoadingbar * innerLoadingdbar);


    let innerP = percentage.querySelector(".inner-percentage");
    innerP.innerHTML = procent + "%";


})



const mouseDownHandler = function(e) {

    imgContainer.style.cursor = 'grabbing';

    imgContainer.style.userSelect = 'none';

    pos = {

        left: imgContainer.scrollLeft,

        x: e.clientX,

    };



    document.addEventListener('mousemove', mouseMoveHandler);

    document.addEventListener('mouseup', mouseUpHandler);

}

const mouseMoveHandler = function(e) {

    const dx = e.clientX - pos.x;

    imgContainer.scrollLeft = pos.left - dx;

};


const mouseUpHandler = function() {

    imgContainer.style.cursor = 'grab';

    imgContainer.style.removeProperty('user-select');



    document.removeEventListener('mousemove', mouseMoveHandler);

    document.removeEventListener('mouseup', mouseUpHandler);

};

// imgContainer.addEventListener('mousedown', mouseDownHandler);

navToggle.addEventListener("click", function(){
    if(!nav.classList.contains("mobile")){
        nav.classList.add("mobile");
    }else{
        nav.classList.remove("mobile");
    }

    if(!nav.classList.contains("scroll")){
        nav.classList.add("scroll");
        logo.style.cssText = "color:white;"
        font.style.cssText  = "color:white"

        scrollLinks.forEach(function(link){
            link.classList.add("white");
        })
    }else{
        nav.classList.remove("scroll");
        // logo.style.cssText = "color:black;"
        // font.style.cssText  = "color:black"

        scrollLinks.forEach(function(link){
            link.classList.remove("white");
        })
    }
})


window.addEventListener('scroll', function(){
    var y = window.scrollY;
    // console.log(y);
    if(y > 50){
        nav.classList.add("scroll");
        logo.style.cssText = "color:white;"
        font.style.cssText  = "color:white"

        scrollLinks.forEach(function(link){
            link.classList.add("white");
        })

    }else{
        nav.classList.remove("scroll");
        font.style.cssText  = "color:white"

        scrollLinks.forEach(function(link){
            link.classList.remove("white");
        })
    }
})

// projects.forEach(function(project){
//     project.addEventListener("click", function(){
//         const id = project.dataset.modal;
//         addModal(id);
//     })
// })

buttons.forEach(function(button){
    button.addEventListener("click", function(){
        modals.forEach(function(modal){
            modal.classList.remove("modal-block")
        })      
        overlay.style.display = 'none';
    })
})


overlay.addEventListener("click", function(){
    removeModal();
});

// function addModal(id){
//     const modal = document.getElementById(id);
//     modal.classList.add("modal-block");
//     overlay.style.display = 'block';
// }

function removeModal(){
    modals.forEach(function(modal){
        modal.classList.remove("modal-block")
    })
    overlay.style.display = 'none';
}


//smooth scroll
scrollLinks.forEach(function(link){
    link.addEventListener("click", function(e){
        e.preventDefault();

        const id = e.currentTarget.getAttribute('href').slice(1);
        const element = document.getElementById(id);

        let position = element.offsetTop - navHeight;

        window.scrollTo({
            left:0,
            top: position,
        });

        if(nav.classList.contains("mobile")){
            nav.classList.remove("mobile");
        }
    })
})

// heroBtn.addEventListener("click", function(e){
//     e.preventDefault();

//     const id = e.currentTarget.getAttribute('href').slice(1);
//     const element = document.getElementById(id);
//     const navHeight = nav.getBoundingClientRect().height;

//     let position = element.offsetTop - navHeight;


//     window.scrollTo({
//         left:0,
//         top:position,
//     });
// })
